const mysql = require("mysql");
const logger = require('../logger/log')
const debugdb = require("debug")("oauth2.dbhelper");
const debugerror = require("debug")("oauth2.dberror");
const config = require("../oauth-config/config");
//object which holds the connection to the db

 global.oauth_pool = null;


function initPool(){
    if(global.oauth_pool ===null){
        debugdb("initalizing global pool connection");
        global.oauth_pool  = mysql.createPool({
            connectionLimit : 5,
            waitForConnections: true,
            connectTimeout: 5000,
            acquireTimeout: 5000,
            host: config.database.host,
            user: config.database.user,
            password: config.database.password,
            database: config.database.database,
            port: config.database.port
        });   
    }else{
        debugdb("already initalizing global pool connection set");
    }
  
}
async function executeQuery(queryString) {
    return new Promise(function (resolve, reject) {
        // debugdb(queryString);
        global.oauth_pool.getConnection(function(err, connection){
            if (err) {
                debugerror(err);
                logger.info("database Connection error"+err);
                reject(err);
            } else {
                connection.query(queryString, function (error, results) {
              
                    connection.destroy()
                    if (error) {
                        logger.info("database  error"+err);
                        reject(error);
                    }else{
                        resolve(results);
                    }
                })
            }

        });
    });
}
module.exports.executeQuery = executeQuery;
module.exports.initPool = initPool;
