/**
 * exports middleware module.exports.authorize = function(req, res, next);
 * requires config section to be setup with database, clients 
 * const oauth2 = require("./x/y/oauth2");
 * simply add the middleware to the paths that you want to secure.
 * app.use("/verify", oauth2.authorize, oauth2Router.verify);
 */




const config = require('./oauth-config/config.js')


const debug = require("debug")("oauth2");
const debugcontroller = require("debug")("oauth2controller");
const debugerror = require("debug")("oauth2error");
const debugmodel = require("debug")("oauth2model");
const uuid_g = require('uuid/v4')
var crypto = require('crypto');

const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json')
// const http = require('http');

var express = require('express');
const mysql = require("mysql")
let executeQuery;



var router = express.Router();

router.post('/authorization_code', AuthorizationCodeHandler);
router.post("/token", TokenHanlder);

module.exports = router;
module.exports.oauth2app = oauth2app;
module.exports.authorize = Authorize;
module.exports.verify = VerifyPingHandler;
module.exports.config = config;
/**
 * Hanlder to deal with authorization code api call
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * req.body must have 
 * @param client_id - unique client id issued by the backend team for the App
 * @param response_type - use "code"
 * @param code_challenge - an encrypted random number generated using SHA256 
 * @param code_challenge_method - S256 or Plain 
 * @param state - optionally provided and then returned in all responses further 
 * @returns {}  on error return error and error_description
 * invalid_request - if missing parameters
 * invalid_client - if wrong client_id
 * unauthorized_client – This client is not authorized to use the requested grant type.
 * unsupported_grant_type – If a grant type is requested that the authorization server doesn’t recognize
 */
function AuthorizationCodeHandler(req, res, next) {
    //HTTP headers to ensure clients do not cache this request.
    debug("AuthorizationCodeHandler called")
    res.set({
        "Cache-Control": "no-store",
        "Pragma": "no-cache"
    });
    debug(JSON.stringify(req.body, false, 8));
    //verify if the request has parameters
    if (req.body.client_id === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "client_id not found"
        }).end();
    } else if (req.body.response_type === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "response_type not found"
        }).end();
    } else if (req.body.code_challenge === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "code_challenge not found"
        }).end();
    } else if (req.body.response_type !== "code") {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "invalid response_type"
        }).end();
        code_challenge_method
    } else if (req.body.code_challenge_method === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "code_challenge_method not found"
        }).end();
    } else if (req.body.code_challenge_method.toUpperCase() !== "S256" && req.body.code_challenge_method.toUpperCase() !== "PLAIN") {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "invalid code_challenge_method. uses S256 or Plain"
        }).end();
    } else {
        //all good, now try to validate the client id
        let self = this;
        controller.getClient(req.body.client_id).then((client_record) => {
            const redirect_uri = client_record.redirect_uri ? client_record.redirect_uri : null;
            const code = generateUUID4();
            debugcontroller(code);
            controller.saveCode(code, req.body.client_id, req.body.code_challenge, req.body.code_challenge_method, req.body.state === undefined ? null : req.body.state)
                .then(() => {
                    // now return redirect uri
                    const resp = {
                        state: req.body.state,
                        code: code
                    }
                    if (redirect_uri) resp.redirect_uri = redirect_uri;
                    res.json(resp).end();
                }).catch((error) => {
                    debugcontroller(error);
                    res.status(500).json({
                        status: "error",
                        error: error,
                        error_description: "System Error occurrred"
                    }).end();
                })
        }).catch((error) => {
            if (error) {
                debug("client_id not found", error);
                res.status(400).send({
                    status: "error",
                    error: "invalid_client",
                    error_description: "client_id not found"
                }).end();
            } else {
            }
        });
    }

}
/**
 * 
 * @param {*} req 
 * client_id 
 * grant_type
 * @param {*} res 
 * @param {*} next 
 */
function TokenHanlder(req, res, next) {
    //HTTP headers to ensure clients do not cache this request.
    debug("CodeHandler called")
    res.set({
        "Cache-Control": "no-store",
        "Pragma": "no-cache"
    });
    //verify if the request has parameters
    if (req.body.client_id === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "client_id not found"
        }).end();
    } else if (req.body.grant_type === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "grant_type not provided"
        }).end();
    } else if (req.body.grant_type !== "authorization_code") {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "invalid grant_type only authorization_code allowed"
        }).end();
    } else if (req.body.code === undefined) {
        let x = req.body.code;
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "code not provided"
        }).end();
    } else if (req.body.verifier_code === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "verifier_code not provided"
        }).end();
    } else if (req.body.code_challenge_method === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "code_challenge_method not found"
        }).end();
    } else if (req.body.code_challenge_method.toUpperCase() !== "S256" && req.body.code_challenge_method.toUpperCase() !== "PLAIN") {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "invalid code_challenge_method. uses S256 or Plain"
        }).end();
    } else {
        //all good, now try to validate the code id
        let self = this;
        // debug(JSON.stringify(req.body));
        controller.verifyCode(req.body.state, req.body.client_id, req.body.code, req.body.verifier_code, req.body.code_challenge_method).then((results) => {
            res.json(results).end();
        }).catch((error) => {
            debug("code not found", error);
            res.status(400).send({
                status: "error",
                error: "invalid_code",
                error_description: "code verification failed"
            }).end();
        });
    }
}
function Authorize(req, res, next) {
    //check for valid bearer token
    const bearer = req.headers['authorization'] || req.headers['Authorization'];
    if (bearer === undefined) {
        res.status(401).json({
            status: "error",
            error: "invalid_request",
            error_description: "Authorization Required"
        }).end();
    } else {
        //check the bearer token validity
        if (bearer.indexOf("Bearer") !== 0) {
            res.status(400).json({
                status: "error",
                error: "invalid_request",
                error_description: "Authorization header not correct. Bearer XXXXXX format required"
            }).end();
        } else {
            const token = bearer.substr(7).trim();
            if (token.length === 0) {
                res.status(400).json({
                    status: "error",
                    error: "invalid_request",
                    error_description: "Authorization header not correct. Bearer XXXXXX token missing"
                }).end();
            } else {
                //all good now let us find it in database
                controller.getAccessToken(token).then((token) => {
                    debug("Authorized:", token);
                    req.token = token;
                    next();
                }).catch((error) => {
                    debugerror(error);
                    res.status(401).json({
                        status: "error",
                        error: "invalid_token",
                        error_description: "Invalid token"
                    }).end();
                })
            }
        }
    }
}
/**
 * Allows sample code to verify if authorisation is working.
 * The response checks Authorization: Bearer XXXXXXX header to contain valid token
 * If valid it returns "Authorized" as response.
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function VerifyPingHandler(req, res, next) {
    res.status(200).json({
        token: req.token
    }).end();
}
class AccessTokenController {
    constructor(accessTokenModel) {
        this.accessToken_M = accessTokenModel;
    }

    token(req, res) {

    }

    getClient(clientId) {
        let self = this;
        return new Promise((resolve, reject) => {
            // debugcontroller("getClient: clientId: %s", clientId);
            self.accessToken_M.getClient(clientId).then((client_record) => {
                resolve(client_record);
            }).catch((error) => {
                reject(error);
            })
        })
    }
    grantTypeAllowed(grantType, callback) {
        if (grantType == "authorization_code") {
            callback(null);
            debug("grantTypeAllowed: clientId: %s, grantType:%s", clientId, grantType);
        } else {
            callback("Grant type not allowed");
            debug("grantTypeAllowed: clientId: %s, grantType:%s", clientId, grantType);
        }
    }
    verifyCode(state, client_id, code, verifier_code, code_challenge_method) {
        let self = this;
        if (state === undefined) state = null;
        return new Promise((resolve, reject) => {
            const code_challenge = (code_challenge_method.toUpperCase() === 'S256') ? getCodeChallenge(verifier_code) : verifier_code;
            self.accessToken_M.verifyCode(state, client_id, code, code_challenge).then((results) => {
                // debug(JSON.stringify(results));
                if (results.length === 1) {
                    //generate access token and return
                    const accessToken = {
                        code: code,
                        client_id: results[0].client_id,
                        access_token: generateUUID4(),
                        expires_in: config.expiresIn
                    }
                    self.accessToken_M.saveAccessToken(accessToken.code, accessToken.client_id, accessToken.access_token, accessToken.expires_in).then((res) => {
                        debug(JSON.stringify(res));
                        if (res.affectedRows === 1) {
                            resolve(accessToken);
                        } else {
                            reject("access token save failed");
                        }
                    }).catch((error) => {
                        reject(error);
                    })
                } else {
                    reject("code verification failed")
                }
            }).catch((error) => {
                debug(error);
                reject("technical verification failed")
            })
        });
    }
    saveCode(code, clientId, code_challenge, code_challenge_method, state, callback) {
        let self = this;
        return new Promise((resolve, reject) => {
            let codeJson = {
                "code": code,
                "clientId": clientId,
                "codeChallenge": code_challenge,
                "codeChallengeMethod": code_challenge_method,
                "state": state
            }
            debugcontroller("saveCode: code: %s clientId: %s code_challenge: %s state: %s", code, clientId, code_challenge, state);
            self.accessToken_M.saveCode(codeJson).then((result) => {
                resolve(result);
            }).catch((error) => {
                reject(error);
            });
        })
    }
    getAccessToken(bearerToken, callback) {
        let self = this;
        return new Promise((resolve, reject) => {
            this.accessToken_M.getAccessToken(bearerToken).then((results) => {
                if (results.length > 0)
                    resolve(results[0])
                else
                    reject("Bearer token not valid");
            }).catch((error) => {
                debugerror(error);
                reject(error);
            });
        });
    }
}
class AccessTokenModel {
    constructor() {
    }
    saveCode(codeJson, callback) {
        let self = this;
        return new Promise((resolve, reject) => {
            let _newcodeJson = codeJson;
            let query = "INSERT INTO ?? (??,??,??,??,??) VALUES (?,?,?,?,?)"
            let inserts = [config.tables.codes, 'code', 'code_challenge', 'code_challenge_method', 'client_id', 'state', _newcodeJson.code, _newcodeJson.codeChallenge, _newcodeJson.codeChallengeMethod, _newcodeJson.clientId, _newcodeJson.state]
            query = mysql.format(query, inserts)
            debug(query);
            //execute the query to get the user
            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    deleteCode(code, callback) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = "DELETE FROM ?? WHERE ??=?";
            let inserts = [config.tables.codes, 'code', code];
            query = mysql.format(query, inserts)
            //execute the query to get the user
            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    verifyCode(state, client_id, code, code_challenge, callback) {
        let self = this;
        return new Promise((resolve, reject) => {

            let query = "SELECT * FROM ?? WHERE ??=? AND ??=? AND ??=? AND used = false";
            // add state to query to compare if passed
            if (state) query += " AND ??=?"
            if (config.codeLifetimeSeconds > 0) {
                //only allow code from last 10 seconds to be used for token request
                query += ` AND created_date > date_add(NOW(), INTERVAL -${config.codeLifetimeSeconds} SECOND)`;
            }
            let inserts = [config.tables.codes, 'client_id', client_id, 'code', code, 'code_challenge', code_challenge];
            //add state to query to compare if passed
            if (state) inserts = [config.tables.codes, 'client_id', client_id, 'code', code, 'code_challenge', code_challenge, 'state', state];
            query = mysql.format(query, inserts)
            //execute the query to get the user
            executeQuery(query).then((results) => {
                if (results.length === 1) {
                    //mark as used
                    query = "UPDATE ?? SET used = true WHERE ??=? AND ??=?";
                    inserts = [config.tables.codes, 'code', code, 'code_challenge', code_challenge];
                    query = mysql.format(query, inserts);
                    executeQuery(query).then((res) => {
                        if (res.affectedRows === 1) debug("marked code as used");
                        resolve(results);
                    }).catch((error) => {
                        reject(error);
                    })
                } else {
                    self.deleteCode(code).then((results) => {
                        if (results.affectedRows === 1) {
                            reject("security: cleared previous code if it exists");
                        } else {
                            reject("code verification failed");
                        }

                    }).catch((error) => {
                        reject(error)
                    });
                }
            }).catch((error) => {
                reject(error);
            });
        });
    }
    saveAccessToken(code, client_id, access_token, expires, callback) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = `INSERT INTO ?? (??,??,??,??) VALUES (?,?,?,DATE_ADD(NOW(), INTERVAL ${config.expiresIn} SECOND))`;
            let inserts = [config.tables.tokens, 'code', 'client_id', 'token', 'expires', code, client_id, access_token,]
            query = mysql.format(query, inserts)
            //execute the query to get the user
            executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    getAccessToken(bearerToken, callback) {
        let self = this;
        return new Promise((resolve, reject) => {
            var doesTokenExistQuery = "SELECT * FROM ?? WHERE ??=? AND ??>NOW()"
            var inserts = [config.tables.tokens, 'token', bearerToken, 'expires']
            doesTokenExistQuery = mysql.format(doesTokenExistQuery, inserts)
            //execute the query to get the user
            executeQuery(doesTokenExistQuery).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    getClient(clientId) {
        let self = this;
        debugmodel("get client called")
        return new Promise((resolve, reject) => {
            debugmodel("get client promise called")
            var getClientQuery = "SELECT * FROM ?? WHERE ??=?"
            var inserts = [config.tables.clients, 'client_id', clientId]
            getClientQuery = mysql.format(getClientQuery, inserts)
            //execute the query to get the user
            executeQuery(getClientQuery).then((results) => {
                if (results.length > 0) {
                    resolve(results[0]);
                } else {
                    reject("Incorrect client_id");
                }
            }).catch((error) => {
                reject("Incorrect client_id");
            })
        })
    }

}
function generateUUID4() {
    let d = Math.random() * 100000000000000 + "";
    return uuid_g(d, config.defaultnamespace)
}
/**
 * Verify the database connection & ensure tables exist
 * If not, create them automatically
 */

async function ensureClient(client) {
    let query = "SELECT * FROM ?? WHERE client_id = ?";
    let inserts = [config.tables.clients, client.client_id];
    query = mysql.format(query, inserts);
    try {
        let results = await executeQuery(query);

        if (results.length === 0) {
            //create this client
            query = "INSERT INTO ?? (client_id, name, redirect_uri) VALUES (?,?,?)";
            inserts = [config.database.database + '.' + config.tables.clients, client.client_id, client.name, client.redirect_uri];
            query = mysql.format(query, inserts);
            try {
                results = await executeQuery(query);
                if (results.affectedRows !== 1) {
                    const msg = `client creation failed : ${client.client_id} - ${client.name}`;
                    throw new Error(msg);
                }
            } catch (error) {
                // debugerror(error);                 
            }

        }
    } catch (error) {
        // debugerror(error);
    }
}
async function verifyDatabase() {
    debug("verifying oauth2 database setup");
    //check clients table
    const createClientSql = `CREATE TABLE IF NOT EXISTS ${config.tables.clients} (
        id int(11) NOT NULL AUTO_INCREMENT,
        client_id varchar(100) NOT NULL,
        name varchar(45) DEFAULT NULL,
        redirect_uri varchar(255) DEFAULT '/oauth/code',
        PRIMARY KEY (id),
        UNIQUE KEY oauth2_client_client_id_unique (client_id),
        KEY oauth2_client_client_id_idx (client_id)        
      ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
    `;

    const createTokenSql = `CREATE TABLE IF NOT EXISTS ${config.tables.tokens} (
        id int(11) NOT NULL AUTO_INCREMENT,
        token varchar(100) NOT NULL,
        code varchar(100) DEFAULT NULL,
        client_id varchar(100) NOT NULL,
        expires datetime NOT NULL,
        created_date datetime DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (id),
        UNIQUE KEY oauth2_token_token_unique (token),
        KEY oauth2_token_token_idx (token),
        KEY oauth2_token_code_idx (code),
        KEY oauth2_token_client_id_idx (client_id),
        CONSTRAINT oauth2_token_client_id_fk FOREIGN KEY (client_id) REFERENCES ${config.tables.clients} (client_id) ON DELETE CASCADE
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    `;
    const createCodeSql = `CREATE TABLE IF NOT EXISTS ${config.tables.codes} (
        id int(11) NOT NULL AUTO_INCREMENT,
        code varchar(100) DEFAULT NULL,
        code_challenge varchar(500) NOT NULL,
        code_challenge_method varchar(10) NOT NULL,
        client_id varchar(100) NOT NULL,
        state varchar(100) DEFAULT NULL,
        used int(11) DEFAULT '0',
        created_date datetime DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (id),
        UNIQUE KEY oauth2_code_code_unique (code),
        KEY oauth2_code_code_challenge_idx (code_challenge,code),
        KEY oauth2_code_client_id_idx (client_id),
        CONSTRAINT oauth2_code_client_id_fk FOREIGN KEY (client_id) REFERENCES ${config.tables.clients} (client_id) ON DELETE CASCADE
      ) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
    `;
    try {
        debug("verifying client table");
        await executeQuery(createClientSql);
    } catch (error) {
        debugerror(error);
        throw error;
    }
    try {
        debug("verifying code table");
        await executeQuery(createCodeSql);
    } catch (error) {
        debugerror(error);
        throw error;
    }
    try {
        debug("verifying token table");
        await executeQuery(createTokenSql);
    } catch (error) {
        debugerror(error);
        throw error;
    }
    for (let index = 0; index < config.clients.length; index++) {
        //create clients if not exist
        try {
            await ensureClient(config.clients[index]);
        } catch (error) {
           // console.log("here");
            debugerror(error);
            throw error;
        }
    }
}

function base64URLEncode(code) {
    return code.toString('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=/g, '');
}
function getVerifierCode() {
    const code = crypto.randomBytes(32);
    const verifier_code = base64URLEncode(code);
    return verifier_code;
}

function getCodeChallenge(code) {
    debug(code);
    return base64URLEncode(crypto.createHash('sha256').update(code).digest());
}

const controller = new AccessTokenController(new AccessTokenModel());

/**
 * Oauth2Server
 */
/**
 * Get port from environment and store in Express.
 */

// var port = normalizePort(process.env.RPA_AGENT_PORT || config.oauthPort || '9090');
// oauth2app.set('port', port);
// app.use('/oauth2', router);
// app.use('/oauth2/verify', module.exports.authorize, module.exports.verify);
// app.use('/api-docs',swaggerUi.serve, swaggerUi.setup(swaggerDocument));

async function oauth2app(options) {
    if(options.executeQuery !== undefined){
        executeQuery = options.executeQuery;
    }else{
        const db = require("./sqlhelper/sqlhelper");
        executeQuery = await db.executeQuery
    }

        verifyDatabase().then(() => {
            debug("Database setup verified");
        }).catch((error) => {
            debugerror("Database Setup Failed")
            process.exit();
        });

    if (options.app !== undefined) {
        options.app.use('/oauth2', router);
        options.app.use('/oauth2/verify', module.exports.authorize, module.exports.verify);
        options.app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    } else {
        if (options.port !== undefined) {

            const compression = require('compression');
            const cors = require('cors');
            const http = require('http');
            const app = express();
            app.use(express.json());
            app.use(express.urlencoded({ extended: false }));
            const cookieParser = require('cookie-parser');
            app.use(cookieParser());

            const bodyParser = require('body-parser')
            const jsonParser = bodyParser.json({ limit: '10mb' }) //{
            const urlEncoded = bodyParser.urlencoded({ limit: '10mb', extended: true }) //

            app.set('superSecret', config.secret)
            app.disable("x-powered-by")
            app.use(cors())
            app.use(jsonParser)
            app.use(urlEncoded)
            app.use(compression())


            var port = normalizePort(process.env.RPA_AGENT_PORT || options.port);

            app.set('port', port);
            app.use('/oauth2', router);
            app.use('/oauth2/verify', module.exports.authorize, module.exports.verify);
            app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
            var server = http.createServer(app);

            server.listen(options.port);
            server.on('error', onError);
            server.on('listening', onListening);

            /**
             * Normalize a port into a number, string, or false.
             */

            function normalizePort(val) {
                var port = parseInt(val, 10);

                if (isNaN(port)) {
                    // named pipe
                    return val;
                }

                if (port >= 0) {
                    // port number
                    return port;
                }

                return false;
            }

            /**
             * Event listener for HTTP server "error" event.
             */

            function onError(error) {
                debug(error);
                if (error.syscall !== 'listen') {
                    throw error;
                }

                var bind = typeof port === 'string'
                    ? 'Pipe ' + port
                    : 'Port ' + port;

                // handle specific listen errors with friendly messages
                switch (error.code) {
                    case 'EACCES':
                        console.error(bind + ' requires elevated privileges');
                        process.exit(1);
                        break;
                    case 'EADDRINUSE':
                        console.error(bind + ' is already in use');
                        process.exit(1);
                        break;
                    default:
                        throw error;
                }
            }

            /**
             * Event listener for HTTP server "listening" event.
             */

            function onListening() {
                var addr = server.address();
                var bind = typeof addr === 'string'
                    ? 'pipe ' + addr
                    : 'port ' + addr.port;
                debug('Oauth2 Server Listening on ' + bind);
            }


        } else {
            throw Error("Oauth2 Requires Port in options");
        }
    }

};

