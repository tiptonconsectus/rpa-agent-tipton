crypto = require('crypto');
http = require("http");
querystring = require("querystring");
config = require("../../config/config");
debug = require("debug")("consectus.oauth:");
debugerror = require("debug")("consectus.oautherror:");
module.exports = class OauthToken {
    constructor(host = "localhost", port = 9089, renewBeforeMilliseconds = 5000) {
        this.verifier_code = null;
        this.code_challenge = null;
        this.oauth2Host = host;
        this.oauth2Port = port;
        this.renewBeforeMilliseconds = renewBeforeMilliseconds;
        this.code = null;
        this.token = null;
        this.state = null;
        this.expires_in = null;
        this.expires = null;
    }
    async getAccessToken(fresh = false) {
        //if token exists check if the expires time is in next 2 seconds
        if (this.token && !fresh && (this.expires-new Date().valueOf())>this.renewBeforeMilliseconds) {
            return this.token;
        } else {
            //get fresh token
            try {
                this.code = await this.getAuthorizationCode();
                debug("Authorization Code: " + this.code)
                this.token = await this.getToken();
                debug("Token: " + this.token);
                return this.token;
            } catch (error) {
                this.code = null;
                this.token = null;
                throw error;
            }
        }
    }
    verify() {
        let self = this;
        return new Promise(function (resolve, reject) {
            if (self.token) {
                const postData = querystring.stringify({
                    test: 'consectus-rpa'
                });
                const options = {
                    hostname: self.oath2Host,
                    port: self.oauth2Port,
                    path: '/oauth2/verify',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Authorization': 'Bearer ' + self.token,
                        'Content-Length': Buffer.byteLength(postData)
                    }
                };
                const req = http.request(options, (res) => {
                    res.setEncoding('utf8');
                    res.on('data', (chunk) => {
                        let data = JSON.parse(chunk);
                        debug(JSON.stringify(data, false, 4));
                        resolve(data.token);
                    });
                    res.on('end', () => {
                        // debug('No more data in response.');
                    });
                });
                req.on('error', (e) => {
                    debugerror(`problem with request: ${e.message}`);
                    reject(e);
                });
                // write data to request body
                req.write(postData);
                req.end();
            } else {

            }
        });

    }
    async getToken() {
        let self = this;
        return new Promise(function (resolve, reject) {
            const postData = querystring.stringify({
                state: self.state,
                code: self.code,
                verifier_code: self.verifier_code,
                grant_type: "authorization_code",
                code_challenge_method: "S256",
                client_id: 'consectus-rpa'
            });
            // debug(postData);
            const options = {
                hostname: self.oauth2Host,
                port: self.oauth2Port,
                path: '/oauth2/token',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': Buffer.byteLength(postData)
                }
            };
            // {
            //     "code": "c411c047-7d12-4d51-a354-8fed9f6ae6d9",
            //     "client_id": "consectus-rpa",
            //     "access_token": "fcb43f24-7072-4089-bd39-75284379a646",
            //     "expires_in": "7200"
            // }
            const req = http.request(options, (res) => {
                let data;
                // debug(`STATUS: ${res.statusCode}`);
                // debug(`HEADERS: ${JSON.stringify(res.headers)}`);
                res.setEncoding('utf8');
                res.on('data', (chunk) => {
                    try{
                        data = JSON.parse(chunk);
                  
                        // /console.log(data)
                        // debug(`BODY: ${JSON.stringify(data, false, 4)}`);
                        // debug(data.access_token);
                        self.token = data.access_token;
                        self.expires_in = data.expires_in;
                    
                    //  self.expires =new Date(new Date().valueOf()+ parseFloat(self.expires_in));
                        self.expires =new Date().valueOf()+ parseFloat(self.expires_in);
                        // setTimeout(() => {
                        //     //request for accessToken in expires_in seconds;
                        //     self.token = null;
                        //     self.getAccessToken(true);
                        // }, data.expires_in - 2000);
                        // debug("Refreshing Token in "+(data.expires_in-2000)+" seconds")
                        resolve(self.token);
                    }catch(err){
                        debugerror(`problem in try catch json parse line 177(util/oauth2): ${err}`);
                    }
                });
                res.on('end', () => {
                    // debug('No more data in response.');
                });
            });
            req.on('error', (e) => {
                debugerror(`problem with request: ${e.message}`);
                reject(e);
            });
            // write data to request body
            req.write(postData);
            req.end();
        });
    }
    async getAuthorizationCode() {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.verifier_code = getVerifierCode();
            self.code_challenge = getCodeChallenge(self.verifier_code);
            self.state = getVerifierCode(); // some random string useful to verify when token is requested.
            const postData = querystring.stringify({
                state: self.state,
                code_challenge: self.code_challenge,
                response_type: "code",
                code_challenge_method: "S256",
                client_id: 'consectus-rpa'
            });
            const options = {
                hostname: self.oauth2Host,
                port: self.oauth2Port,
                path: '/oauth2/authorization_code',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Length': Buffer.byteLength(postData)
                }
            };
            // {
            //     "state": "abcd",
            //     "code": "af42b25f-d863-4501-a3b1-613a13b7c24c"
            // }
            const req = http.request(options, (res) => {
                let data;
                // debug(`STATUS: ${res.statusCode}`);
                // debug(`HEADERS: ${JSON.stringify(res.headers)}`);
                res.setEncoding('utf8');
                res.on('data', (chunk) => {
                    try{
                         data = JSON.parse(chunk);
                            
                        // debug(`BODY: ${JSON.stringify(data, false, 4)}`);
                        resolve(data.code);
                    }catch(err){
                        debugerror(JSON.stringify(chunk))
                        debugerror(`problem in try catch json parse line 177(util/oauth2): ${err}`);
                    }
                   
                });
                res.on('end', () => {
                    // debug('No more data in response.');
                });
            });
            req.on('error', (e) => {
                debugerror(`problem with request: ${e.message}`);
                reject(e);
            });
            // write data to request body
            req.write(postData);
            req.end();
        });
    }

}
function base64URLEncode(code) {
    return code.toString('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=/g, '');
}
function getVerifierCode() {
    const code = crypto.randomBytes(32);
    const verifier_code = base64URLEncode(code);
    return verifier_code;
}
function getCodeChallenge(code) {
    return base64URLEncode(crypto.createHash('sha256').update(code).digest());
}
