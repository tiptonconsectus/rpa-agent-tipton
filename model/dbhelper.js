const debugdb = require("debug")("dbhelper:");
const debugerror = require("debug")("dbhelper-error:");
const mysql = require("mysql");
const config = require("../config/config");
//object which holds the connection to the db

global.pool = null;

function initPool() {
    if (global.pool === null) {
        debugdb("initializing global pool connection");
        global.pool = mysql.createPool({
            connectionLimit: 8,
            waitForConnections: true,
            connectTimeout: 5000,
            acquireTimeout: 5000,
            multipleStatements: true,
            host: config.database.host,
            user: config.database.user,
            password: config.database.password,
            database: config.database.database,
            port: config.database.port

        });
    } else {
        debugdb("initializing global pool already set");
    }

}
initPool();
async function executeQuery(queryString) {
    return new Promise(function (resolve, reject) {
        debugdb("execute Query:", queryString);
        global.pool.getConnection(function (err, connection) {
            if (err) {
                debugerror(err);
                reject(err);
            } else {
                connection.query(queryString, function (error, results) {

                    connection.destroy()
                    if (error) {
                        // debugerror(error);
                        reject(error);
                    } else {
                        resolve(results);
                    }
                })
            }

        });
    });
}
module.exports.executeQuery = executeQuery;
module.exports.mysql = mysql;

// console.log(config);
async function initialize() {
    //create db
    const createDb = 'create database if not exists ' + config.database.database;

    //check to ensure that all the database tables are correctly created
    //TYPE nodejs or script 
    const createPathsSql = `CREATE TABLE IF NOT EXISTS ${config.tables.paths} (
        id int(11) NOT NULL AUTO_INCREMENT,
        version int(11) DEFAULT 0,
        name varchar(100) NOT NULL,
        type varchar(20) NOT NULL DEFAULT 'nodejs',
        queued TINYINT NOT NULL,
        PRIMARY KEY (id),
        order_id  int(2) NOT NULL DEFAULT 99,
        created_on datetime DEFAULT CURRENT_TIMESTAMP,
        UNIQUE KEY paths_name_unique (name),
        KEY paths_name_idx (name)       
      ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
    `;
    const createPathQueuesSql = `CREATE TABLE IF NOT EXISTS ${config.tables.queues} (
        id int(11) NOT NULL AUTO_INCREMENT,
        path_id int(11) NOT NULL, 
        serverqueue  int(11) NOT NULL,
        params text,
        status int(11) NOT NULL DEFAULT 0,
        result text,
        callback_url text,
        created_on datetime DEFAULT CURRENT_TIMESTAMP,
        updated_on datetime,
        PRIMARY KEY (id),
        KEY paths_path_idx (path_id),        
        CONSTRAINT queues_paths_id_fk FOREIGN KEY (path_id) REFERENCES ${config.tables.paths} (id) ON DELETE CASCADE
      ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
    `;
    const createArchiveQueuesSql = `CREATE TABLE IF NOT EXISTS ${config.tables.queue_archive} (
        id int(11) NOT NULL AUTO_INCREMENT,
        path_id int(11) NOT NULL, 
        params text,
        status int(11) NOT NULL DEFAULT 0,
        result text,
        created_on datetime DEFAULT CURRENT_TIMESTAMP,
        updated_on datetime,
        PRIMARY KEY (id),
        KEY archive_path_idx (path_id),        
        CONSTRAINT archive_queue_paths_id_fk FOREIGN KEY (path_id) REFERENCES ${config.tables.paths} (id) ON DELETE CASCADE
      ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
    `;
    const createSettingsSql = `CREATE TABLE IF NOT EXISTS ${config.tables.settings} (
        id int(11) NOT NULL AUTO_INCREMENT,
        name varchar(100) NOT NULL,
        value text,
        PRIMARY KEY (id),
        UNIQUE KEY settings_name_unique (name)
    ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
    `;
    const settingsSql = [];
    settingsKeys = Object.keys(config.settings)
    for (i = 0; i < settingsKeys.length; i++) {
        const nameSql = mysql.format(`SELECT value FROM ?? WHERE ??=?`, [config.tables.settings, 'name', settingsKeys[i]]);

    }
    try {
        await executeQuery(createDb);
        await executeQuery(createPathsSql);
        debugdb(createPathQueuesSql);
        await executeQuery(createPathQueuesSql);
        await executeQuery(createArchiveQueuesSql);
        await executeQuery(createSettingsSql);
        settingsKeys = Object.keys(config.settings)
        for (i = 0; i < settingsKeys.length; i++) {
            sql = mysql.format(`SELECT value FROM ?? WHERE ??=?`, [config.tables.settings, 'name', settingsKeys[i]]);
            result = await executeQuery(sql);
            if (result.length === 0) {
                sql = mysql.format(`INSERT INTO ?? (??, ??) VALUES (?,?)`, [config.tables.settings, 'name', 'value', settingsKeys[i], config.settings[settingsKeys[i]]]);
                await executeQuery(sql);
                debugdb(`added key ${settingsKeys[i]}: ${config.settings[settingsKeys[i]]}`);
            }
        }
        debugdb("verified database -------------------------")

    } catch (error) {
        debugdb(error);
    }

}
module.exports.initialize = initialize;
