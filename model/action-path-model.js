const debug = require("debug")("action-path-model:");
const config = require("../config/config");
const dbhelper = require("../model/dbhelper");
const mysql = require("mysql");
class ActionPathModel {
    constructor(model) {
        this.name = (model === undefined) ? null : model.name;
        this.version = (model === undefined) ? null : model.version;
        this.flow = (model === undefined) ? null : model.flow;
    }
    getActionPathByName(name, version = null) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = "SELECT * FROM ?? ";
            query += (version === null) ? " WHERE ??=? LIMIT 1 " : " WHERE ??=? AND version=" + version + " LIMIT 1 "
            let inserts = [config.tables.paths, 'name', name]
            query = mysql.format(query, inserts)
            // debug(query);
            //execute the query to get the user
            dbhelper.executeQuery(query).then((results) => {
                // debug(results);
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    getActionPathById(id) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = "SELECT * FROM ?? WHERE ??=? LIMIT 1";
            let inserts = [config.tables.paths, 'id', id]
            query = mysql.format(query, inserts)
            // debug(query);
            //execute the query to get the user
            dbhelper.executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    getActionPaths() {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = "SELECT * FROM ?? ORDER BY created_on DESC";
            let inserts = [config.tables.paths]
            query = mysql.format(query, inserts)
            // debug(query);
            //execute the query to get the user
            dbhelper.executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    updateActionPath(path, version = null) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = null;
            this.getActionPathByName(path.name).then((results) => {
                query = null;
                if (results.length === 0) {
                    query = (version === null) ? `INSERT INTO ?? (version, ??, ??,??) VALUES(1,?,?,?)` : `INSERT INTO ?? (version, ??, ??,??) VALUES(${version},?,?,?)`
                    let update = [config.tables.paths,
                        'name', 'type', 'queued', path.name, path.type, path.queued
                    ];
                    query = mysql.format(query, update)
                } else {
                    query = (version === null) ? `UPDATE ?? SET ??=?,version=version+1, ??=?, ??=? WHERE ??=?` : `UPDATE ?? SET ??=?,version=${version}, ??=?, ??=? WHERE ??=?`;
                    let update = [
                        config.tables.paths,
                        'name', path.name,
                        'type', path.type,
                        'queued', path.queued,
                        'id', results[0].id];
                    query = mysql.format(query, update)
                }
                // debug(query);
                //execute the query to get the user
                dbhelper.executeQuery(query).then((results) => {
                    resolve(results);
                }).catch((error) => {
                    reject(error);
                });
            }).catch((error) => {
                reject(error);
            })
        });
    }
    deleteActionPath(path) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = null;
            if (path.id === undefined || path.id === null) {
                reject("path id not found. nothing to delete")
            } else {
                //update
                query = `DELETE FROM ?? WHERE ??=?`;
                let update = [config.tables.paths, 'id', path.id];
                query = mysql.format(query, update);
            }
            // debug(query);
            //execute the query to get the user
            dbhelper.executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    addQueue(path, status, params, callback, serverqueueid) {
        let self = this;
        return new Promise((resolve, reject) => {
            // debug(params);
            let query1 = " SELECT * FROM ?? where serverqueue =?";
            let update1 = [config.tables.queues, serverqueueid];
            query1 = mysql.format(query1, update1)
            dbhelper.executeQuery(query1).then((checkqueue) => {
                if (checkqueue.length === 0) {
                    let query = `INSERT INTO ?? (??, ??, ??,?? ,?? ) VALUES (?, ?, ?,? , ?)`
                    let update = [config.tables.queues,
                        'path_id', 'params', 'status', 'callback_url', 'serverqueue',
                    path.id, JSON.stringify(params), status, JSON.stringify(callback), serverqueueid];
                    query = mysql.format(query, update)
                    // debug(query);
                    //execute the query to get the user
                    dbhelper.executeQuery(query).then((results) => {
                        resolve(results);
                    }).catch((error) => {
                        reject(error);
                    });

                } else {
                    let queue_Id = { insertId: checkqueue[0].id };

                    resolve(queue_Id);
                }

            }).catch((error) => {
                reject(error);
            });

        });
    }
    updateQueue(queueId, status, result) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = null;
            query = `UPDATE ?? SET ??=?, ??=?, ??=NOW() WHERE ??=?`
            let update = [config.tables.queues,
                'status', status,
                'result', result,
                'updated_on',
                'id', queueId];
            query = mysql.format(query, update)
            // debug(query);
            //execute the query to get the user
            dbhelper.executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    getQueue(path = null, limit = 1000, offset = null) {
        debug(path);
        let self = this;
        return new Promise((resolve, reject) => {
            let query;
            let inserts;
            if (path !== null) {
                query = "SELECT * FROM ?? WHERE ??=? ORDER BY created_on DESC LIMIT 100";
                inserts = [config.tables.queues, 'path_id', path.id]
            } else {
                query = "SELECT * FROM ?? WHERE (status BETWEEN 0 AND 1)  LIMIT " + limit + " OFFSET " + offset;
                inserts = [config.tables.queues]
            }
            query = mysql.format(query, inserts)
            // debug(query);
            //execute the query to get the user
            dbhelper.executeQuery(query).then((results) => {
                // debug(results);
                resolve(results);
            }).catch((error) => {
                debug(error);
                reject(error);
            });
        });
    }
    getQueueAction(limit) {

        //    let query = "SELECT url.url  FROM ?? role INNER JOIN ?? url ON role.url_id=url.id WHERE ??=? "
        return new Promise((resolve, reject) => {
            let query = "SELECT queue.*,action.name,action.type FROM ?? ";
            query += " queue INNER JOIN ?? action ON queue.path_id=action.id  "
            query += " WHERE queue.status BETWEEN 0 AND 1  ORDER BY order_id "

            query += " LIMIT " + limit

            let inserts = [config.tables.queues, config.tables.paths];
            query = mysql.format(query, inserts)
            //   console.log(query)
            // debug(query);
            //execute the query to get the user
            dbhelper.executeQuery(query).then((count) => {
                resolve(count);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    getActionPathQueueById(name, queue_id) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = `SELECT 
                ??.*, 
                ??.name as path_name, ??.type as path_type, ??.queued as path_queued
                FROM ??, ?? WHERE ??.??=??.?? AND ??.??=? AND ??.??=? LIMIT 1`;
            let inserts = [
                config.tables.queues,
                config.tables.paths, config.tables.paths, config.tables.paths,
                config.tables.queues,
                config.tables.paths,
                config.tables.queues, 'path_id',
                config.tables.paths, 'id',
                config.tables.paths, 'name', name,
                config.tables.queues, 'id', queue_id];
            query = mysql.format(query, inserts)
            // console.log(query)
            // debug(query);
            //execute the query to get the user
            dbhelper.executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    getQueueById(queue_id) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = "SELECT * FROM ?? WHERE ??=? LIMIT 1";
            let inserts = [config.tables.queues, 'id', queue_id]
            query = mysql.format(query, inserts)
            // debug(query);
            //execute the query to get the user
            dbhelper.executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    getPendingQueue() {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = `SELECT 
                ??.*, 
                ??.name as path_name, ??.type as path_type, ??.queued as path_queued
                FROM ??, ?? WHERE ??.??=??.?? AND ??.??=? ORDER BY created_on ASC LIMIT 1000`;
            let inserts = [
                config.tables.queues,
                config.tables.paths, config.tables.paths, config.tables.paths,
                config.tables.queues,
                config.tables.paths,
                config.tables.queues, 'path_id',
                config.tables.paths, 'id',
                config.tables.queues, 'status', 0];
            query = mysql.format(query, inserts)
            // debug(query);
            //execute the query to get the user
            dbhelper.executeQuery(query).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    archiveQueue(queueId, status, result) {
        let self = this;
        return new Promise((resolve, reject) => {
            let query = `INSERT INTO ?? SELECT * FROM ?? WHERE ??.??>?`;
            let update = [
                config.tables.queue_archive,
                config.tables.queues,
                config.tables.queues, 'status', 0];
            query = mysql.format(query, update)
            debug(query);
            //execute the query to get the user
            dbhelper.executeQuery(query).then((results) => {
                if (results.affectedRows > 0) {
                    //now delete 
                    query = `DELETE FROM ?? WHERE ??>?`;
                    update = [config.tables.queues, 'status', 0];
                    query = mysql.format(query, update)
                    debug(query);
                    dbhelper.executeQuery(query).then((delResults) => {
                        resolve(results);
                    }).catch((error) => {
                        reject(error);
                    });
                }
                resolve(results);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    orderupdate(name, value) {
        // console.log(name)
        // console.log(value)
        return new Promise((resolve, reject) => {
            let query;
            for (var i = 0; i < name.length; i++) {
                query = " UPDATE ?? SET order_id=" + value[i] + " WHERE name='" + name[i] + "'"

                let inserts = [config.tables.paths]
                query = mysql.format(query, inserts)
                // console.log(query)

                dbhelper.executeQuery(query).then((update) => {
                    resolve(update);
                }).catch((error) => {
                    reject(error);
                });
            }

        })
    }
}
module.exports = ActionPathModel;