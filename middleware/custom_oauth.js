const config = require('../config/config')
module.exports=function(req,res,next){
    const bearer = req.headers['authorization'] || req.headers['Authorization'];
    if (bearer === undefined ) {
        
            res.status(401).json({
                status: "error",
                error: "invalid_request",
                error_description: "Authorization Required"
            }).end();
    } else if(bearer ===config.callback_key) {
        next()
    }else{
        res.status(401).json({
            status: "error",
            error: "invalid_request",
            error_description: "Authorization Required"
        }).end();
    }
}