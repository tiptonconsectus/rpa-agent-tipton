var express = require('express');
var router = express.Router();
const Ping = require("../controller/manage/ping");
/* GET users listing. */
module.exports =  (app) => {
  //route for registering new users
  //upload api
  router.post("/ping", Ping.Getping);    
  router.post("/update/setting", Ping.UpdateSetting);
  router.post("/update/order", Ping.UpdateOrder);     

  return router;
}

