var express = require('express');
var router = express.Router();
const UploadActionPath = require("../controller/tasks/upload-action-path");
const GetActionPaths = require("../controller/tasks/get-action-paths");
const ExecuteActionPath = require("../controller/tasks/execute-action-path");
const GetActionPathQueue = require("../controller/tasks/get-action-path-queue");
const DeleteActionPath = require("../controller/tasks/delete-action-path");
const GetQueueStatus = require("../controller/tasks/get-action-path-queue-status");
const GetPendingQueue = require("../controller/tasks/get-peding-queue");
const ArchiveQueue = require("../controller/tasks/archive-queue");
const custom_oauth = require('../middleware/custom_oauth')
/* GET users listing. */
module.exports =  (app) => {
  //route for registering new users
  //upload api
  router.post("/upload-action-path", app.oauth2.authorize, app.upload.single('action-path-zip'), UploadActionPath);  
  router.post("/get-action-paths", app.oauth2.authorize, GetActionPaths);
  router.post("/get-action-path-queue", app.oauth2.authorize, GetActionPathQueue);
  router.post("/get-action-path-queue-status", app.oauth2.authorize, GetQueueStatus);
  router.post("/delete-action-path", app.oauth2.authorize, DeleteActionPath);
  router.post("/execute-action-path", app.oauth2.authorize, ExecuteActionPath);
  router.post("/get-pending-queue", app.oauth2.authorize, GetPendingQueue);
  router.post("/archive-queue", app.oauth2.authorize, ArchiveQueue);

  router.post("/upload-action-path/process", app.oauth2.authorize, app.upload.single('action-path-zip'), UploadActionPath);  
  router.post("/execute-action-path/process", app.oauth2.authorize, ExecuteActionPath);
  return router;
}

