"use strict";
const debug = require("debug")("consectus-rpa:java");

exports.getJavaInstance = function () {
    var fs = require("fs");
    var path = require("path");
    var java = require("java");
    var baseDir = __dirname + "/java-lib";
    var dependencies = fs.readdirSync(baseDir);

    for (let dependency of dependencies) {
        if (path.extname(dependency) === ".jar") {
            //      debug("pushed: %s/%s", baseDir, dependency);
            java.classpath.push(baseDir + "/" + dependency);
        }
    }
    java.options.push('-Djava.awt.headless=false');
    java.options.push('-Dsikuli.Debug=0');
    java.callStaticMethodSync("org.sikuli.basics.Debug", "quietOn");
    return java;
}