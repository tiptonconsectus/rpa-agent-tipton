const http = require("http");
const querystring = require("querystring");
let infinte = true
//let counter=0
const Action_M = require("../model/action-path-model");
const action_m = new Action_M();
// const httprequest =require('../services/utils/httprequest');
const path = require('path');
const fs = require('fs');
const decache = require('decache');
// const http=new httprequest();
const debug = require("debug")("execute-action-path:");
const debugerr = require("debug")("error in process");
// const decache = require('decache');
const config = require("../config/config.js");
const GetSetting = require("../model/setting-model");
const Setting = new GetSetting();
const actionPath = path.join(__dirname, "..", config.action_path_location);
let tryCount = 0;
let tryTimes = 2;
// const StepCallback = require("./action-paths/callbackstep/callback");
class MonitorProcess {
    constructor(OauthToken = null, SendEmail = null) {
        this.OauthToken = OauthToken;
        this.sendEmail = SendEmail;
        this.isBusy = false;
    }
    start() {
        try {
            setInterval(this.Monitor.bind(this), config.processInterval)
        } catch (e) {
            debugerr("error in set interval", e)
        }
    }
    async Monitor() {
        if (!this.isBusy) {
            this.isBusy = true;
            debug("Starting agent processing")
            let limit = 1;
            let exec
            try {
                exec = await action_m.getQueueAction(limit);
                debug(exec)
                if (exec.length > 0) {
                    for (let j = 0; j < exec.length; j++) {
                        try {
                            await this.executeAction(exec[j], j);
                        } catch (error) {
                            debugerr(error);
                        }
                    }
                } else {
                    debug("nothing to execute");
                }
            } catch (err) {
                debugerr(err)
                // this.isBusy = false;
                throw err;
            }
            debug("Finished agent processing");
            this.isBusy = false;
        } else {
            debug("Process busy will try after " + (config.processInterval / 1000) + " seconds");
        }
    }
    async  executeAction(exec, j) {
        debug(exec.name)
        debug(exec.type)
        let action;
        let filename = path.join(actionPath, exec.name);
        if (!fs.existsSync(filename)) {
            //   console.log("j3 " +j)
            debug(filename, 'failed');
            debug("folder not found in action paths");
            throw new Error("folder not found in action paths");
        } else {
            // decache(filename);
            debug("filefound");
            debug(filename);
            let req = {}
            let res = {}
            if (exec.type.toLowerCase() === 'nodejs') {
                debug("nodejs");
                const ActionPath = require(filename);
                try {
                    action = new ActionPath(req, res);
                    debug("action", action);
                } catch (error) {
                    debugerr("file error ", error)
                    throw error;
                }
            } else {
                const script = fs.readFileSync(path.join(actionPath, exec.name, 'index.script'), { encoding: 'utf8' });
                action = new ActionPathClass(req, res);
                action.parse(script);
            }
            let params = JSON.parse(exec.params);
            params.callbackpath = JSON.parse(exec.callback_url);
            debug(params)
            let response = await action.execute(params);
            debug(response)
            let status = 2; // execution finished
            if (response.status === undefined) {
                status = -1;
            } else if (response.status === 'error') {
                status = -1;
                if (response.data.sopraerror && response.data.sopraerror === 'newtork error occured') {
                    status = 0;
                    tryCount = tryCount + 1;
                    if (tryCount >= tryTimes) {
                        tryCount = 0;
                        status = -1;
                    }
                }
            } else if (response.status === '-1') {
                status = 0;
                tryCount = tryCount + 1;
                if (tryCount >= tryTimes) {
                    tryCount = 0;
                    status = -1;
                }
            }
            try {
                if (status === -1) {
                    // response.data.sopraerror ===                    
                    let toemail = await Setting.getSetting('adminEmail');
//console.log(response.data.sopraerror, "response.data.sopraerror")
                    let emailinfo = {
                        to: toemail[0] && toemail[0].value ? toemail[0].value : "demo@consectus.com",
                        message: response.data.sopraerror,
                        agentname: (process.env.DATABASE) ? process.env.DATABASE : "rpa-agent1"
                    }
                    let errinfo = {
			  message: response.data.sopraerror,
                        agentname: (process.env.DATABASE) ? process.env.DATABASE : "rpa-agent1",
                        path :exec.name ? exec.name : "failed to find name",
                        image: response.data.img,
                    }
                    this.sendEmail(emailinfo, errinfo);
                }
                await action_m.updateQueue(exec.id, status, JSON.stringify(response));
            } catch (err) {
                debugerr(err)
                throw err;
            }
            if (status !== 0) this.ServerQueueUpdate(exec, response, status), tryCount = 0;
            delete require.cache[require.resolve(filename)];
            return response;
        }
    }
    ServerQueueUpdate(exec, result, status) {
        debug("ServerQueueUpdate")
        /**
         * params also have callback url check line number 100
         */
        const url = JSON.parse(exec.callback_url)
        debug(url)
        const postData = querystring.stringify({
            response: JSON.stringify(result),
            queueid: exec.id,
            status: status
        });
        debug(postData)
        let oauth = this.OauthToken(url.host, url.port);
        oauth.getAccessToken(false).then(() => {
            const options = {
                hostname: url.host,
                port: url.port,
                path: url.path,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': 'Bearer ' + oauth.token,
                    'Content-Length': Buffer.byteLength(postData)
                }
            };
            // console.log(options)
            this.HttpPost(options, postData).then((result) => {
                debug(result)
            }).catch((err) => {
                debugerr(err)
            })
        }).catch((error) => {
            debugerr("error in ininininininini", error)
        })
    }
    HttpPost(options, postData) {
        return new Promise((resolve, reject) => {
            const req = http.request(options, (res) => {
                //  const { statusCode } = res;
                let data;
                let error = (res.statusCode !== 200);
                res.setEncoding('utf8');
                res.on('data', (chunk) => {
                    try {
                        debug(chunk)
                        data = JSON.parse(chunk);
                    } catch (e) {
                        debugerr("e.message");
                        debugerr(e.message);
                        reject("error in try method")
                    }
                    //   data += chunk;
                    //use here resolve
                });
                res.on('end', () => {
                    // const parsedData = data;
                    // const parsedData = data;
                    if (error) {
                        reject(data)
                    } else {
                        resolve(data)
                    }
                    // debug('No more data in response.');
                });
            })
            req.on('error', (e) => {
                debugerr("e")
                debugerr(e)
                debugerr(`Got error: ${e.message}`);
                // debugerror(`problem with request: ${e.message}`);
                reject(e);
            });
            // write data to request body
            req.write(postData);
            req.end();
        })
    }
}
module.exports = MonitorProcess;