const path = require("path");
ActionPath = require("../../lib/action-path");
let sopraLogin = require("../sopralogin/index");
let loginsopra = new sopraLogin();
//const config = require("../../config")
let sopra = "demo1";
let trytimes = 7;
let waitforimage = 60; //sec
let waitinloop = 10;
class TRDP extends ActionPath {
    constructor(req, res) {
        super(req, res);
    }
    async execute(params) {
        try {
            if (!params.batchno) params.batchno = "" + Math.floor(Math.random() * 1000000) + "";

            params.subaccno = "1";
            params.source = params.source ? params.source : "DUD";
            params.gbltype = params.gbltype ? params.gbltype : "W";
            params.payment = "IT"
            if (params.checknba) {
                params.payment = "T"
            }
            //  let params = {
            //                 batchno: ""+Math.floor(Math.random()*1000000)+"",
            //                 society:"1",
            //                 source:"DUD",
            //                 date:"25-FEB-2019",
            //                 accno:"98000103",
            //                 subaccno: "1",
            //                 gbltype:"r",
            //                 payment:"DD",
            //                 effdate:"26-FEB-2019",
            //                 amt:"100",
            //                 reference:"salary",
            //                 nominated:"sdsdsdsd",
            //                 // nextbatch:"true"

            //              }
            params.nextbatch = true
            if (sopra !== "demo") {
                let login = loginsopra.ensureSopraScreen();
                if (login === 1) {
                    let menu = this.wait(this.getPattern(path.join(__dirname, "./trans/menuprompt.png"), 0.90, 0, 0), waitforimage);
                    this.type('TRDP', menu);
                    this.keys("\n");
                    this.keys("\n");
                    let tdp; let tdpdark;
                    let batchno;
                    let society;
                    let source;
                    let date;
                    let entertrans;
                    let transdate;
                    let accno;
                    let subaccno;
                    let gbltype;
                    let payment;
                    let effdate;
                    let amount;
                    let reference;

                    let favmsg;
                    for (let i = 0; i < trytimes; i++) {
                        favmsg = this.wait(this.getPattern(path.join(__dirname, "./trans/favmsg.png"), 0.90, 0, 0), 30);
                        if (favmsg) break;
                        if (trytimes - 1 === i) throw "IN TRDP Image :-./trans/favmsg.png  not found Develpoer:- favmsg -" + favmsg + " tries time:-" + i;
                    }
                    for (let i = 0; i < trytimes; i++) {
                        tdp = this.wait(this.getPattern(path.join(__dirname, "./trans/tdp.png"), 0.90, 0, 0), 30);
                        tdpdark = this.wait(this.getPattern(path.join(__dirname, "./trans/tdpdark.png"), 0.90, 0, 0), 30);
                        if (tdp || tdpdark) {

                            break;
                        }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/tdp.png, ./trans/tdpdark.png  not found Develpoer:- tdp -" + tdp + " tdpdark-" + tdpdark + " tries time:-" + i;
                    }
                    for (let i = 0; i < trytimes; i++) {
                        batchno = this.wait(this.getPattern(path.join(__dirname, "./trans/batchno.png"), 0.90, 10, 0), 30);
                        this.highlight(batchno)
                        if (batchno) {
                            break;
                        }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/batchno.png  not found Develpoer:- batchno -" + batchno + " tries time:-" + i;
                    }
                    this.type(params.batchno);

                    // for(let i=0;i<trytimes;i++){
                    //     society= this.wait(this.getPattern(path.join(__dirname, "./trans/society.png"),0.90,16,0));
                    //     this.highlight(society);
                    //     if(society){
                    //         break;
                    //     }
                    //     if(trytimes-1 === i) throw new Error("acvmenu  not found line 57") 
                    // }
                    // this.type(params.society,society);
                    for (let i = 0; i < trytimes; i++) {
                        source = this.wait(this.getPattern(path.join(__dirname, "./trans/source.png"), 0.90, 0, 0));
                        this.highlight(source);
                        let sourcetype = this.type(params.source, source);
                        if (source && sourcetype) {

                            break;
                        }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/source.png  not found Develpoer:- source -" + source + " tries time:-" + i;
                    }

                    for (let i = 0; i < trytimes; i++) {
                        date = this.wait(this.getPattern(path.join(__dirname, "./trans/date.png"), 0.90, 0, 0));
                        if (date) {

                            break;
                        }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/date.png  not found Develpoer:- date -" + date + " tries time:-" + i;
                    }
                    this.type(params.date, date);

                    //  let checkdetail= this.wait(this.getPattern(path.join(__dirname, "./trans/checkdetail.png"),0.90,62,-2));
                    // this.type('OB',checkdetail);

                    //-------------------------------------------------------------------------//             
                    for (let i = 0; i < trytimes; i++) {
                        entertrans = this.wait(this.getPattern(path.join(__dirname, "./trans/entertrans.png"), 0.90, 0, 0));
                        this.click(entertrans);
                        transdate = this.wait(this.getPattern(path.join(__dirname, "./trans/transdate.png"), 0.90, 0, 0));
                        if (transdate) {
                            break;
                        }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/entertrans.png, ./trans/transdate.png   not found Develpoer:- entertrans -" + entertrans + " transdate-" + transdate + " tries time:-" + i;
                    }


                    this.type(params.date, transdate);
                    this.keys("\t");
                    let okalertpopup = this.wait(this.getPattern(path.join(__dirname, "./trans/daterr.png"), 0.90, 0, 0), 10);
                    let tranerror = this.wait(this.getPattern(path.join(__dirname, "./trans/tranerror.png"), 0.90, 0, 0), 10);

                    if (okalertpopup && tranerror) {
                        for (let i = 0; i < trytimes; i++) {
                            okalertpopup = this.wait(this.getPattern(path.join(__dirname, "./trans/daterr.png"), 0.90, 0, 0), 10);
                            let clickalert = this.click(okalertpopup)
                            let okalertpopup1 = this.wait(this.getPattern(path.join(__dirname, "./trans/daterr.png"), 0.90, 0, 0));
                            if (!okalertpopup1 && clickalert) break;
                        }
                        let toveriride;
                        for (let i = 0; i < trytimes; i++) {
                            let toveriridenew = this.wait(this.getPattern(path.join(__dirname, "./trans/toveriride.png"), 0.90, 0, 0));
                            if (toveriridenew) {
                                toveriride = toveriridenew
                            } else {
                                toveriride = this.wait(this.getPattern(path.join(__dirname, "./trans/highightoverride.png"), 0.90, 0, 0));
                            }
                            let overideclick = this.click(toveriride);
                            if (overideclick && i > 1) break;
                        }
                    }

                    for (let i = 0; i < trytimes; i++) {
                        let accno1 = this.wait(this.getPattern(path.join(__dirname, "./trans/accno.png"), 0.90, 0, 0));

                        if (accno1) { accno = accno1; }
                        else {
                            accno = this.wait(this.getPattern(path.join(__dirname, "./trans/trnsaccno2.png"), 0.90, 0, 0));
                        }
                        this.highlight(accno);
                        if (accno) { break; }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/accno.png, ./trans/trnsaccno2.png   not found Develpoer:- accno -" + accno + " accno1-" + accno1 + " tries time:-" + i;
                    }
                    this.type(params.account_number, accno);
                    for (let i = 0; i < trytimes; i++) {
                        subaccno = this.wait(this.getPattern(path.join(__dirname, "./trans/subaccno.png"), 0.90, 20, 0));
                        if (subaccno) {
                            break;
                        }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/subaccno.png   not found Develpoer:- subaccno -" + subaccno + " tries time:-" + i;

                    }
                    this.type(params.subaccno, subaccno);
                    for (let i = 0; i < trytimes; i++) {
                        gbltype = this.wait(this.getPattern(path.join(__dirname, "./trans/gbltype.png"), 0.90, 30, 0));
                        if (gbltype) { break; }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/gbltype.png   not found Develpoer:- gbltype -" + gbltype + " tries time:-" + i;
                    }
                    this.type(params.gbltype, gbltype);
                    for (let i = 0; i < trytimes; i++) {
                        payment = this.wait(this.getPattern(path.join(__dirname, "./trans/payment.png"), 0.90, 30, 0));
                        if (payment) { break; }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/payment.png   not found Develpoer:- payment -" + payment + " tries time:-" + i;

                    }
                    this.type(params.payment, payment);
                    this.keys("\t");
                    let transaccdtl = this.wait(this.getPattern(path.join(__dirname, "./trans/transaccdtl.png"), 0.90, 0, 0), 30);
                    if (transaccdtl) {
                        this.type(params.transferaccount);
                        this.keys("\t");
                        this.type(params.subaccno);
                        this.keys("\t");
                        this.keys("\t");
                        //    for(let i=0;i<trytimes;i++){
                        //     let  tranotoverride  =  this.wait(this.getPattern(path.join(__dirname, "./trans/tranotoverride.png"),0.90,0,0),10);
                        //     let clicktranotoverride =this.click(tranotoverride);
                        //     let  tranotoverride1  =  this.wait(this.getPattern(path.join(__dirname, "./trans/tranotoverride.png"),0.90,0,0),10);
                        //     if(tranotoverride1 && clicktranotoverride) break;
                        //     if(trytimes-1 === i) throw "  not found line 160";
                        //    }
                    }



                    for (let i = 0; i < trytimes; i++) {
                        if (this.wait(this.getPattern(path.join(__dirname, "./trans/transamout1.png"), 0.90, 0, 0))) {
                            amount = this.wait(this.getPattern(path.join(__dirname, "./trans/transamout1.png"), 0.90, 0, 0))
                        } else {
                            amount = this.wait(this.getPattern(path.join(__dirname, "./trans/amount.png"), 0.90, 0, 0));
                        }
                        if (amount) {

                            break;
                        }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/transamout1.png, ./trans/amount.png  not found Develpoer:- amount -" + amount + " tries time:-" + i;

                    }
                    this.type(params.amount, amount);
                    if (!params.checknba) {
                        for (let i = 0; i < trytimes; i++) {
                            reference = this.wait(this.getPattern(path.join(__dirname, "./trans/reference.png"), 0.90, 0, 0));
                            if (amount) {

                                break;
                            }
                            if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/reference.png  not found Develpoer:- reference -" + reference + " tries time:-" + i;
                        }
                        this.type(params.reference, reference);
                    }

                    for (let i = 0; i < trytimes; i++) {
                        let posttrans = this.wait(this.getPattern(path.join(__dirname, "./trans/posttrans.png"), 0.90, 0, 0));
                        if (posttrans) {
                            let posttrnsclick = this.click(posttrans);
                            if (posttrnsclick) break;
                        }
                        if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/posttrans.png  not found Develpoer:- posttrans -" + posttrans + " tries time:-" + i;
                    }
                    let warningtrans = this.wait(this.getPattern(path.join(__dirname, "./trans/warningtrans.png"), 0.90, 0, 0), 30);
                    if (warningtrans) {
                        for (let i = 0; i < trytimes; i++) {
                            let postdatealert = this.wait(this.getPattern(path.join(__dirname, "./trans/postdatealert.png"), 0.90, 0, 0));
                            this.click(postdatealert);
                            let postdatealert1 = this.wait(this.getPattern(path.join(__dirname, "./trans/postdatealert.png"), 0.90, 0, 0));
                            if (!postdatealert1) break;
                            if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/postdatealert.png  not found Develpoer:- postdatealert -" + postdatealert + " tries time:-" + i;
                        }
                    }

                    //-------------------------------------------------------------------------//
                    if (params.nextbatch) {
                        let close;
                        let menutrdp;
                        for (let i = 0; i < trytimes; i++) {
                            let batchdone = this.wait(this.getPattern(path.join(__dirname, "./trans/batchdone.png"), 0.90, 0, 0));
                            let clickbatch = this.click(batchdone);
                            close = this.wait(this.getPattern(path.join(__dirname, "./trans/close.png"), 0.90, 0, 0));
                            if (clickbatch && close) { break; }
                            if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/batchdone.png, ./trans/close.png  not found Develpoer:- batchdone -" + batchdone + " close-" + close + " tries time:-" + i;
                        }
                        for (let i = 0; i < trytimes; i++) {
                            close = this.wait(this.getPattern(path.join(__dirname, "./trans/close.png"), 0.90, 0, 0));
                            let closeclick = this.click(close);
                            menutrdp = this.wait(this.getPattern(path.join(__dirname, "./trans/menutrdp.png"), 0.90, 0, 0), waitforimage);
                            if (closeclick && menutrdp) { break; }
                            if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/close.png, ./trans/menutrdp.png  not found Develpoer:- close -" + close + " menutrdp-" + menutrdp + " tries time:-" + i;
                        }
                        for (let i = 0; i < trytimes; i++) {
                            menutrdp = this.wait(this.getPattern(path.join(__dirname, "./trans/menutrdp.png"), 0.90, 0, 0), waitforimage);
                            if (menutrdp) {
                                for (let i = 0; i < 5; i++) {
                                    this.keys("\b");
                                }
                                break;
                            }
                            if (trytimes - 1 === i) throw "IN TRDP Image :- ./trans/menutrdp.png  not found Develpoer:- menutrdp -" + menutrdp + " tries time:-" + i;
                        }
                    }


                    return {
                        status: "ok", message: "successfully", data: params
                    }
                }
                else {
                    params.sopraerror = "newtork error occured";
                    return {
                        status: "error",
                        data: params,
                        message: "rpa_agent_failed"
                    }

                }
            } else {
                setTimeout(function () { return { status: "ok", data: params } }, 10000);
            }

        } catch (error) {
            try {
                let datetime = new Date().getTime();
                let img = error.replace(/\s/g, '').substr(0, 6) + datetime;
                this.capture(img);
            } catch (e) { }
            loginsopra.ensureSopraScreen();
            // console.log(error);
            this.debugerror(error);
            params.sopraerror = JSON.stringify(error);
            params.screen = this.screen;
            return {
                status: "error",
                data: params,
                message: "rpa_agent_failed"
            }
        }

    }
}

// const na = new TRDP();
// na.execute({});
module.exports = TRDP;
//process.exit(0);
