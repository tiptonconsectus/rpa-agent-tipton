const path = require("path");
ActionPath = require("../../lib/action-path");
let sopraLogin = require("../sopralogin/index");
let loginsopra = new sopraLogin();
let trytimes = 7;

let newacc = require("../newaccount/index.js");
let newAcc = new newacc();
let Bcam = require("../BCAM/index.js");
let bcam = new Bcam();
// const config = require("../../config/config")
// config.sopra ="demo";
class CustomerVerfiy extends ActionPath {
    constructor(req, res) {
        super(req, res);
    }
    execute(params, callback = null) {
        try {
            params.accno = (typeof params.account_number === "string") ? params.account_number : "" + params.account_number + "";
            params.accountcode = (params.accountcode) ? params.accountcode : "SF01";
            if (params.ratechangeperiod && params.ratechangeperiod.period) params.ratechangeperiod.period = params.ratechangeperiod.period;
            if (params.ratechangeperiod && params.ratechangeperiod.value) params.ratechangeperiod.value = params.ratechangeperiod.value;
            else params.ratechangeperiod = { period: "D", value: "30" }

            if (params.noticeperiod && params.noticeperiod.period) params.noticeperiod.period = params.noticeperiod.period;
            if (params.noticeperiod && params.noticeperiod.value) params.noticeperiod.value = params.noticeperiod.value;
            else params.noticeperiod = { period: "D", value: "30" }

            let login = loginsopra.ensureSopraScreen();
            if (login === 1) {
                params = newAcc.execute(params, 0, callback);
                // callback.sendStep(params.callbackpath, "5/6");

                // let params= {
                //     cuimclass:"Miscellaneous PIDs",
                //     cuimacc:"cuimacc",
                //     cuimcollected:"",

                // }
                // params.customersid = "156047"; 
                params.cuimclass = "Proof of address";
                params.cuimtype = "Call Credit Search";
                params.cuimreference = params.pid ? params.pid : "";
                params.cuimacc = params.accno;
                // callback.sendStep(params.callbackpath, "6/6");
                let credit = this.checkCallcredit(params);


                if (!credit.chekstatus) {
                    params.creditcheck = "failed due to " + (credit.message) ? credit.message : "unknown reason"
                }
                bcam.execute(params);
                return {
                    status: "ok", message: "successfully", data: params
                }
            } else if (login === -1) {
                try {
                    let datetime = new Date().getTime();
                    params.img = "login" + datetime;
                    this.capture(params.img);
                } catch (e) { }
                this.hardClose("iexplore.exe");
                params.sopraerror = "login failed"
                return {
                    status: "-1", message: "rpa_agent_failed", data: params
                }
            } else if (login === -2) {
                try {
                    let datetime = new Date().getTime();
                    params.img = "Soprainstancefailed" + datetime;
                    this.capture(params.img);
                } catch (e) { }
                this.hardClose("iexplore.exe");
                params.sopraerror = "Sopra instance is not available"
                return {
                    status: "-1", message: "rpa_agent_failed", data: params
                }
            }
            else {
                try {
                    let datetime = new Date().getTime();
                    params.img = "networkerr" + datetime;
                    this.capture(params.img);
                } catch (e) { }
                this.hardClose("iexplore.exe");
                // loginsopra.ensureSopraScreen();
                params.sopraerror = "newtork error occured";
                return {
                    status: "error",
                    data: params,
                    message: "rpa_agent_failed"
                }
            }

        } catch (error) {

            try {
                let datetime = new Date().getTime();
                params.img = error.replace(/\s/g, '').substr(0, 6) + datetime;
                this.capture(params.img);
            } catch (e) { }
            this.hardClose("iexplore.exe");
            loginsopra.ensureSopraScreen();
            // console.log(error);
            params.sopraerror = JSON.stringify(error);
            params.screen = this.getScreen();
            return {
                status: "error",
                data: params,
                message: "rpa_agent_failed"
            }
        }

    }
    checkCallcredit(params) {
        try {
            let menu = this.wait(this.getPattern(path.join(__dirname, "./menuprompt.png"), 0.90, 0, 0), 60);
            this.type('CUIDM', menu);
            this.keys("\n");
            this.keys("\n");
            let documentry;
            let cuimclass;
            let goclearcancel;
            let accnocuim;
            let accdetailcuim;
            for (let i = 0; i < trytimes; i++) {

                let cuimaccbtn = this.wait(this.getPattern(path.join(__dirname, "./cuim/cuimaccbtn.png"), 0.90, 20, 0));
                this.click(cuimaccbtn);
                accnocuim = this.wait(this.getPattern(path.join(__dirname, "./cuim/accnocuim.png"), 0.90, 0, 0), 10);
                if (accnocuim) {
                    break;
                }
                if (trytimes - 1 === i) throw "IN CUIDM Image :-./cuim/cuimaccbtn.png, ./cuim/accnocuim.png not found ,Develpoer:- cuimaccbtn-" + cuimaccbtn + " accnocuim-" + accnocuim + " tries time:-" + i;
            }
            this.type(params.cuimacc, accnocuim)

            for (let i = 0; i < trytimes; i++) {
                this.keys("\t");
                goclearcancel = this.wait(this.getPattern(path.join(__dirname, "./cuim/goclearcancel.png"), 0.90, -62, 0), 30);
                this.click(goclearcancel);
                accdetailcuim = this.wait(this.getPattern(path.join(__dirname, "./cuim/accdetailcuim.png"), 0.90, 0, 0), 2);
                if (!accdetailcuim) break;
                if (trytimes - 1 === i) throw "IN CUIDM Image :-./cuim/goclearcancel.png, ./cuim/accdetailcuim.png not found ,Develpoer:- goclearcancel -" + goclearcancel + " accdetailcuim-" + accdetailcuim + " tries time:-" + i;

            }

            for (let i = 0; i < trytimes; i++) {

                documentry = this.wait(this.getPattern(path.join(__dirname, "./cuim/cuimdocumentry.png"), 0.90, 20, 0), 2);
                this.click(documentry);
                cuimclass = this.wait(this.getPattern(path.join(__dirname, "./cuim/cuimclass.png"), 0.90, 20, 0));
                if (cuimclass) {
                    break;
                }
                if (trytimes - 1 === i) throw "IN CUIDM Image :-./cuim/cuimdocumentry.png , ./cuim/cuimclass.png not found ,Develpoer:- documentry -" + documentry + " cuimclass-" + cuimclass + " tries time:-" + i;
            }
            this.type(params.cuimclass, cuimclass);
            let cuimtype = this.wait(this.getPattern(path.join(__dirname, "./cuim/cuimtype.png"), 0.90, 0, 0), 60);
            this.type(params.cuimtype, cuimtype);
            let cuimreference = this.wait(this.getPattern(path.join(__dirname, "./cuim/cuimreference.png"), 0.90, 0, 0), 60);
            this.type(params.cuimreference, cuimreference);
            let cuimacctype = this.wait(this.getPattern(path.join(__dirname, "./cuim/cuimacc.png"), 0.90, 0, 0), 60);
            this.type(params.cuimacc, cuimacctype);
            // let cuimcollected = this.wait(this.getPattern(path.join(__dirname, "./cuim/cuimcollected.png"), 0.90, 0, 0), 60);
            // this.type(params.cuimcollected,cuimcollected);
            let finalclosebtn;
            let savepopup;
            let menucuidm;
            let closebtnclick = false;

            for (let i = 0; i < trytimes; i++) {
                finalclosebtn = this.wait(this.getPattern(path.join(__dirname, "./cuim/finalclosebtn.png"), 0.90, 0, 0), 10);
                let finalcloseclick = this.click(finalclosebtn);
                savepopup = this.wait(this.getPattern(path.join(__dirname, "./cuim/savepopup.png"), 0.90, -87, 0), 10);
                let okcancelcuim = this.wait(this.getPattern(path.join(__dirname, "./cuim/okcancelcuim.png"), 0.90, -25, 0), 5);

                if ((savepopup || okcancelcuim) && finalcloseclick) {
                    break;
                } else {
                    menucuidm = this.wait(this.getPattern(path.join(__dirname, "./cuim/menucuidm.png"), 0.90, 56, 0), 10);
                    if (menucuidm) {
                        closebtnclick = true;
                        break;
                    }
                }
                if (trytimes - 1 === i) throw "IN CUIDM Image :-./cuim/finalclosebtn.png , ./cuim/savepopup.png, ./cuim/okcancelcuim.png,./cuim/menucuidm.png not found ,Develpoer:- finalclosebtn -" + finalclosebtn + " savepopup-" + savepopup + " okcancelcuim-" + okcancelcuim + " menucuidm-" + menucuidm + " tries time:-" + i;

            }
            // let savepopup = this.wait(this.getPattern(path.join(__dirname, "./cuim/savepopup.png"), 0.90, -87, 0), 30);

            if (savepopup) {
                for (let i = 0; i < trytimes; i++) {
                    let okcancelcuim = this.wait(this.getPattern(path.join(__dirname, "./cuim/okcancelcuim.png"), 0.90, -25, 0), 5);
                    this.click(okcancelcuim);
                    savepopup = this.wait(this.getPattern(path.join(__dirname, "./cuim/savepopup.png"), 0.90, -87, 0), 10);
                    this.click(savepopup);
                    //savepopup = this.wait(this.getPattern(path.join(__dirname, "./cuim/savepopup.png"), 0.90, -87, 0), 10);
                    if (!savepopup) {
                        let okcancelcuim = this.wait(this.getPattern(path.join(__dirname, "./cuim/okcancelcuim.png"), 0.90, -25, 0), 5);
                        if (okcancelcuim) {
                            this.click(okcancelcuim);
                            let okcancelcuim1 = this.wait(this.getPattern(path.join(__dirname, "./cuim/okcancelcuim.png"), 0.90, -25, 0), 2);
                            if (!okcancelcuim1) break;
                        } else {
                            break;
                        }

                    }
                    if (trytimes - 1 === i) throw "IN CUIDM Image :-./cuim/okcancelcuim.png, ./cuim/savepopup.png not found ,Develpoer:- okcancelcuim -" + okcancelcuim + " savepopup-" + savepopup + " tries time:-" + i;
                }
            }
            if (closebtnclick === false) {
                for (let i = 0; i < trytimes; i++) {
                    finalclosebtn = this.wait(this.getPattern(path.join(__dirname, "./cuim/finalclosebtn.png"), 0.90, 0, 0), 10);
                    this.click(finalclosebtn);
                    finalclosebtn = this.wait(this.getPattern(path.join(__dirname, "./cuim/finalclosebtn.png"), 0.90, 0, 0));
                    if (!finalclosebtn) break;
                }
            }
            for (let i = 0; i < trytimes; i++) {
                menucuidm = this.wait(this.getPattern(path.join(__dirname, "./cuim/menucuidm.png"), 0.90, 56, 0), 25);
                if (menucuidm) {
                    for (let i = 0; i < 6; i++) {
                        this.keys("\b");
                    }
                    break;
                }
                if (trytimes - 1 === i) throw "IN CUIDM Image :-./cuim/menucuidm.png not found ,Develpoer:- menucuidm -" + menucuidm + " tries time:-" + i;

            }
            return { chekstatus: true, message: "success" }


        } catch (error) {
            try {
                let datetime = new Date().getTime();
                params.img = error.replace(/\s/g, '').substr(0, 6) + datetime;
                this.capture(params.img);
            } catch (e) { }
            this.hardClose("iexplore.exe");
            loginsopra.ensureSopraScreen();
            return { chekstatus: false, message: error }
        }
    }
}
module.exports = CustomerVerfiy