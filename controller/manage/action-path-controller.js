const ActionPathModel = require("../../model/action-path-model");
module.exports = class ActionPathController {
    constructor() {
        this.actionPathModel = new ActionPathModel();
    }
    getActionPathById(id) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.actionPathModel.getActionPathById(id).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            })
        })
    }
    getActionPathByName(name, version) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.actionPathModel.getActionPathByName(name, version).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            })
        })
    }
    getActionPaths() {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.actionPathModel.getActionPaths().then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            })
        })
    }
    updateActionPath(path, version) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.actionPathModel.updateActionPath(path, version).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            })
        })
    }
    deleteActionPath(path) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.actionPathModel.deleteActionPath(path).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            })
        })
    }
    addQueue(path, status, params, callback, serverqueueid) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.actionPathModel.addQueue(path, status, params, callback, serverqueueid).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            })
        })
    }
    updateQueue(queue, status, result) {
        let self = this;
        return new Promise(function (resolve, reject) {
            // console.log(JSON.stringify(result))
            self.actionPathModel.updateQueue(queue, status, JSON.stringify(result)).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            })
        })
    }
    getQueue(path) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.actionPathModel.getQueue(path).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            })
        })
    }
    getActionPathQueueById(name, queue_id) {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.actionPathModel.getActionPathQueueById(name, queue_id).then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            })
        })
    }
    getPendingQueue() {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.actionPathModel.getPendingQueue().then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            })
        });
    }
    archiveQueue() {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.actionPathModel.archiveQueue().then((results) => {
                resolve(results);
            }).catch((error) => {
                reject(error);
            })
        });
    }
}
