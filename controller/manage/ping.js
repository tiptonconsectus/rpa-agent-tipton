const SettingModel = require("../../model/setting-model");
settingModel = new SettingModel();
const action_path_m = require('../../model/action-path-model');
let action_m = new action_path_m();
function Getping(req, res, next){
    settingModel.getAllSettings().then((results)=>{
        response = {
            status: "OK",
        }
        for (s of results){
            response[s.name]= s.value 
        }
        res.json(response).end();
    }).catch((err)=>{
        console.error(err);
    })
}
  async function checkSetting (req, res, cb){
    try{
        let more_name=JSON.parse(req.body.morename)
        let more_value=JSON.parse(req.body.morevalue)
        let setting=['name','id','setAutoWaitTimeout','matchScore','adminEmail']
        let value=[req.body.name,req.body.id,req.body.timeout,req.body.score,req.body.email]
      
        let more_result;
        for(let i=0;i<setting.length;i++){
           // console.log(value[i])
           more_result=  await settingModel.updateSetting(setting[i],value[i])
        }
        
        if(more_name!==undefined && more_value!==undefined){
            for(let i=0; i<more_name.length;i++){
                more_result= await UpdateSettingFindMore(more_name[i])
                if(more_result.length===0){
                    more_result=   await settingModel.createSetting(more_name[i],more_value[i]);
                }else{
                    more_result= await settingModel.updateSetting(more_name[i],more_value[i]);
                }
            }

        }
    
      return cb(null,{status:200,message:"success"});
    }catch(err){
        // console.log(err)
        return  cb(err);
    }
}
function UpdateSetting(req,res){
    //console.log(req.body)
    checkSetting(req,res,function(err,result){
        if(err){
            res.status(401).send(err).end();
        }else{
            res.status(200).send(result).end();
        }
    })
}

function UpdateSettingFindMore(morename){
    return   new Promise((resolve, reject) => {
        settingModel.getSetting(morename).then((result)=>{
           // console.log(result)
            resolve(result)
        }).catch((err)=>{
            // console.log(err)
            reject(err)
        })
    });

}
function UpdateOrder(req,res){
    console.log(req.body)
    try{
        const name = JSON.parse(req.body.name);
        const value = JSON.parse(req.body.value);
        action_m.orderupdate(name,value).then((results)=>{
            res.status(200).send({status:200,message:"success updated"}).end();
        }).catch((error)=>{
            console.log(error)
            res.status(401).send({status:200,message:"error in  updateorder"}).end();
        })
    }catch(err){
        res.status(401).send({status:200,message:"error in try catch in ping updateorder"}).end(); 
    }


}
module.exports.UpdateSetting=UpdateSetting;
module.exports.Getping=Getping;
module.exports.UpdateOrder=UpdateOrder;