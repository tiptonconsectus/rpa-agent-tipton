
const debug = require("debug")("execute-action-path:");
const path = require("path");
const fs = require("fs");
const config = require("../../config/config");
const decache = require('decache');
const ActionPathController = require("../../controller/manage/action-path-controller");
const actionPathFile = path.join(__dirname, "../..", config.action_path_location);
const actionPathController = new ActionPathController();
const ActionPathClass = require("../../lib/action-path");
module.exports = function (req, res, next) {
    let callback = (req.body.callback !== undefined && req.body.callback !== '' && req.body.callback !== null) ? JSON.parse(req.body.callback) : null
    if (req.body.name === undefined) {
        //name of folder which is stored in db
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "name not found"
        }).end();
    } else if (req.body.name.trim().length === 0) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "name cannot be blank"
        }).end();
    } else if (callback === null) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "callback url is not provided "
        }).end();

    }
    else if (callback.host === undefined || callback.port === undefined || callback.path === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: 'callback url is not provided useage eg:- {"host":"127.1.1.5","port":"8888","path":"callbackpath"}'
        }).end();
    } else if (req.body.params === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "params not found"
        }).end();
    } else if (req.body.params.trim().length === 0) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "params cannot be blank. use {} as empty json object"
        }).end();

    } else {
        debug('here');
        try {
            const name = req.body.name;
            let serverqueueid = req.body.Serverqueueid;
            let params = null;
            let filename;
            try {
                params = JSON.parse(req.body.params);
            } catch (error) {
                debug(error)
                res.status(400).json({
                    status: "error",
                    error: "invalid_request",
                    error_description: "invalid object pass use eg:- {'echo':'test'}"
                }).end();

            }

            let version = (req.body.version !== undefined && req.body.version !== '') ? req.body.version : null

            actionPathController.getActionPathByName(name, parseInt(version)).then((results) => {
                debug(results);
                if (results.length === 0) {
                    res.status(500).json({
                        status: "error",
                        error: "not_found",
                        error_description: "action path not found. please upload latest folder"
                    }).end();
                } else {
                    const actionPath = results[0];
                    let status = 0
                    if (actionPath.queued === 0) {
                        status = 1
                    }

                    actionPathController.addQueue(actionPath, status, params, callback, serverqueueid).then((results) => {
                        const queueId = results.insertId;
                        try {
                            debug("actionPathFile ", actionPathFile)
                            filename = path.join(actionPathFile, name);
                            debug(filename, 'failed');
                            if (!fs.existsSync(filename)) {
                                debug(filename, 'failed');
                                res.status(400).json({
                                    status: "error",
                                    error: "invalid_request",
                                    error_description: "Action path folder not found. please upload latest folder."
                                }).end();
                            } else {
                                let action = null;
                                if (actionPath.queued === 0) {

                                    if (actionPath.type.toLowerCase() === 'nodejs') {
                                        decache(filename);
                                        const ActionPath = require(filename);
                                        action = new ActionPath(req, res);
                                    } else {
                                        //parse the script and execute
                                        const script = fs.readFileSync(path.join(actionPathFile, name, 'index.script'), { encoding: 'utf8' });
                                        action = new ActionPathClass(req, res);
                                        action.parse(script);
                                        action.execute();
                                    }
                                    let response = action.execute(params);
                                    let status = 2; // execution finished
                                    if (response.status === undefined) {
                                        status = -1;
                                    } else if (response.status === 'error') {
                                        status = -1;
                                    }
                                    debug("response", response);
                                    actionPathController.updateQueue(queueId, status, response).then((results) => {
                                        let response_result = {
                                            data: response,
                                            queueid: queueId,
                                            status_type: status
                                        }

                                        res.json(response_result).end();

                                    }).catch((error) => {
                                        debug(error);
                                        res.status(500).json({
                                            status: "error",
                                            error: error,
                                            error_description: "action path queue addition failed"
                                        }).end();
                                    });
                                } else {
                                    let response = {
                                        status: "OK",
                                        queue_id: queueId,
                                        params: params
                                    }
                                    let response_result = {
                                        data: response,
                                        queueid: queueId,
                                        status_type: status
                                    }
                                    res.json(response_result).end();
                                }
                            }
                        } catch (error) {
                            debug(error);
                            res.status(500).json({
                                status: "error",
                                error: error,
                                error_description: "action path exeuction failed"
                            }).end();
                        }
                    }).catch((error) => {
                        debug(error);
                        res.status(500).json({
                            status: "error",
                            error: error,
                            error_description: "action path queue addition failed"
                        }).end();
                    });
                }
            }).catch((error) => {
                debug(error);
                res.status(500).json({
                    status: "error",
                    error: error,
                    error_description: "action path name not found "
                }).end();
            });
        } catch (error) {
            debug(error);
            res.status(500).json({
                status: "error",
                error: error,
                error_description: "some thing went wrong in main catch"
            }).end();
        }
    }
}