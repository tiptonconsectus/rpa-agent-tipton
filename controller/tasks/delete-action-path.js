const debug = require("debug")("get-action-paths:");
const config = require("../../config/config");
const path = require("path");
const ActionPathController = require("../manage/action-path-controller");
const actionPath = path.join(__dirname, "../..", config.action_path_location);

const actionPathController = new ActionPathController();

const fs = require("fs-extra");
module.exports = function (req, res, next) {
    if (req.body.name === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "name not found"
        }).end();
    } else if (req.body.name.trim().length === 0) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "name cannot be blank"
        }).end();
    } else {
        const name = req.body.name;
        actionPathController.getActionPathByName(name).then((results)=>{
            debug(results);
            if (results.length === 0){
                res.status(500).json({
                    status: "error",
                    error: "not_found",
                    error_description: "action path not found"
                }).end();        
            }else{
                const actoinPath = results[0];
                actionPathController.deleteActionPath(actoinPath).then((results) => {
                    //now remove folder
                    try {
                        target = path.join(actionPath, actoinPath.name);
                        debug("deleting: ", target);
                        if (fs.existsSync(target)) fs.removeSync(target);                        
                        let ret = {
                            status: "OK",
                            data: results,
                            message: "action path removed and folder cleared"
                        }                        
                        res.status(200).json(ret).end();
                    } catch (error) {
                        debug(error);
                        let ret = {
                            status: "OK",
                            data: results,
                            message: "folder could not be removed"
                        }                        
                    }
                }).catch((error) => {
                    debug(error);
                    res.status(500).json({
                        status: "error",
                        error: error,
                        error_description: "System Error occurrred"
                    }).end();
                })
                        
            }
        }).catch((error)=>{
            debug(error);
            res.status(500).json({
                status: "error",
                error: error,
                error_description: "System Error occurrred"
            }).end();
        });
    }
}