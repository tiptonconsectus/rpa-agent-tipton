const debug = require("debug")("upload-action-path:");
const debuggerror = require("debug")("upload error:");
const config = require("../../config/config");
const fs = require("fs-extra");
const path = require("path");
const os = require("os");
const unzip = require("unzip-stream");
const actionPath = path.join(__dirname, "../..", config.action_path_location);
const temp = path.join(__dirname,"../..",config.agent_temp);
const ActionPathController = require("../manage/action-path-controller");
const actionPathController = new ActionPathController();
module.exports = function (req, res, next) {
    if (req.body.name === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "name not found"
        }).end();
    } else if (req.body.name.trim().length === 0) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "name cannot be blank"
        }).end();
    } else if (req.body.type === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "type not found"
        }).end();
    } else if (req.body.type.trim().length === 0) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "type cannot be blank"
        }).end();
    } else if (['nodejs', 'script'].indexOf(req.body.type.trim()) < 0) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "wrong type - use 'nodejs' or 'script'"
        }).end();
    } else if (req.body.queued === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "queued not found"
        }).end();
    } else if (req.body.queued.trim().length === 0) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "queued not found"
        }).end();
    } else if (['true', 'false'].indexOf(req.body.queued.trim()) < 0) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "wrong type - use 'true' or 'false'"
        }).end();
    } else {
        //all is well
        const name = req.body.name;
        const type = req.body.type;
        const queued = req.body.queued;
        let target;
        try {
            try {
                fs.ensureDirSync(actionPath);
            } catch (error) {
                debuggerror("ensureDir failed",error)
            }
            debug(req.body);
            const foldername = req.file.path.substr(0, req.file.path.lastIndexOf("."));
            debug("Upload action name: ", name);
            debug("Zip file: ", req.file.path);
            debug("folder name", foldername);
          
            debug("moving");
            debug("source: ", foldername)
            target = path.join(actionPath, path.basename(name));
            debug("target: ", target);
            try {
                if (fs.existsSync(target)) fs.removeSync(target);
                debug(path.join(actionPath, path.basename(name)), 'folder deleted');
            } catch (error) {
                debuggerror(path.join(actionPath, path.basename(name)), "folder does not exist")
            }
            try {
                fs.createReadStream(req.file.path).pipe(unzip.Extract({ path: target }));
                debug(req.file.path, "unzip successful at :-",);
                try{
                    if(fs.existsSync(req.file.path)) fs.removeSync(req.file.path);
                }catch(error){
                    debug(error,"in removesync");
                }
                
                debug(path.join(actionPath, path.basename(name)), 'folder moved');
                data = {
                    name: name,
                    type: type,
                    queued: queued === 'true' ? 1 : 0
                }
                let version = (req.body.version !== undefined && req.body.version !== '' && req.body.version !== null) ? req.body.version : null
                actionPathController.updateActionPath(data, version).then((results) => {
                    res.status(200).json({
                        status: "OK",
                        message: (results.insertId > 0) ? "action path created" : "action path updated",
                        data: results
                    }).end();
                }).catch((error) => {
                    debug(error);
                    res.status(500).json({
                        status: "error",
                        error: error,
                        error_description: "Error updating the action path",
                        name: name
                    }).end();
                })
            } catch (error) {
                debuggerror(path.join(actionPath, path.basename(name)), "could not create folder", error)
                debug(error);
                res.status(500).json({
                    status: "error",
                    error: error,
                    error_description: "Error updating the action path",
                    name: name
                }).end();
            }
        } catch (error) {
            debug(error);
            res.status(500).json({
                status: "error",
                error: error,
                error_description: "Error updating the action path",
                name: name
            }).end();
        }
    }
}