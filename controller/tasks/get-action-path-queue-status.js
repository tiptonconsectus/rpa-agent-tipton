const debug = require("debug")("get-action-paths:");
const ActionPathController = require("../manage/action-path-controller");
const actionPathController = new ActionPathController();
module.exports = function (req, res, next) {
    if (req.body.queue_id === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "queue_id not found"
        }).end();
    } else if (req.body.queue_id.trim().length === 0) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "queue_id cannot be blank"
        }).end();
    } else if (req.body.name === undefined) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "name not found"
        }).end();
    } else if (req.body.name.trim().length === 0) {
        res.status(400).json({
            status: "error",
            error: "invalid_request",
            error_description: "name cannot be blank"
        }).end();
    } else {
            const name = req.body.name;
            const queue_id = req.body.queue_id;
            actionPathController.getActionPathQueueById(name, queue_id).then((results) => {
                debug(results);
                if (results.length === 0) {
                    res.status(500).json({
                        status: "error",
                        error: "not_found",
                        error_description: `queue_id '${queue_id}' not found for action path '${name}'`
                    }).end();
                } else {
                    let ret = {
                        status: "OK",
                        queue: results[0]
                    }
                    res.status(200).json(ret).end();
                }
            }).catch((error) => {
                debug(error);
                res.status(500).json({
                    status: "error",
                    error: error,
                    error_description: "System Error occurrred"
                }).end();
            });
        }
    }