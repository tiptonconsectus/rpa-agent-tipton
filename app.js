
/* Initialize Java Bridge */
const javaInit = require("./bin/javaInit");

const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const compression = require('compression')
const cors = require('cors')
const config = require('./config/config')
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json');
const fs = require('fs');
const temp_folder = path.join(__dirname, "./", config.agent_temp);
const actionPathFolder = path.join(__dirname, "./", config.action_path_location);
// const uuid_g = require("uuid/v4");
const debug = require("debug")("rpa-agent:");
const debugerror = require("debug")("rpa-agent-error:");
const OauthToken = require("./services/utils/oauth2");
const nodemailer = require('nodemailer');

const app = express();
app.debug = debug;
app.debugerror = debugerror;
//required to ensure it works on Macosx.
process.env['JAVA_STARTED_ON_FIRST_THREAD_' + process.pid] = '1'
/* java instance */
app.java = javaInit.getJavaInstance();
//create one instance to avoid later delay in execution
app.screen = app.java.newInstanceSync('org.sikuli.script.Screen');
app.screen.setAutoWaitTimeoutSync(1);

//app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const bodyParser = require('body-parser')
const jsonParser = bodyParser.json({ limit: '10mb' }) //{
const urlEncoded = bodyParser.urlencoded({ limit: '10mb', extended: true }) //

app.transporter = nodemailer.createTransport({
  host: config.nodemailer.smtp,
  // port: config.nodemailer.port,
  port: config.nodemailer.port,
  secure: config.nodemailer.secure, // true for 465, false for other ports
  auth: {
    user: config.nodemailer.email,
    pass: config.nodemailer.passwd
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
});

app.SendEmail = async function (emailinfo, errinfo) {
  //console.log(emailinfo ,"emailinfo" , errinfo , "errinfo")
  let template = path.join(__dirname, 'templates', 'sopramessage.html');
  let texttemplate = path.join(__dirname, 'templates', 'sopramessage.txt');
  let subject = "rpa-bot execution got failed";
  let html;
  let htmltxt;
  let attachments;
  try {
    attachments = [{ filename: 'error.png', path: __dirname + '/sopraerror/' + errinfo.image + ".png", contentType: 'image/png' }];
  } catch (err) {
    attachments = ""
  }
  html = fs.readFileSync(template).toString();
  html = new Function('return (`' + html + '`)').bind(errinfo)();
  htmltxt = fs.readFileSync(texttemplate).toString();
  htmltxt = new Function('return (`' + htmltxt + '`)').bind(errinfo)();
  let emailSend;
  try{
    emailSend = emailinfo.to && emailinfo.to.length ?emailinfo.to :"santosh@consectus.com,michael.skinner@thetipton.co.uk,alisha.kumar@thetipton.co.uk,mark.schofield@thetipton.co.uk"
  }catch(err){
    emailSend = "santosh@consectus.com,michael.skinner@thetipton.co.uk,alisha.kumar@thetipton.co.uk,mark.schofield@thetipton.co.uk"
  }
  let mailJSON = {
    "to": emailSend,   //emailinfo.to,
    "cc": config.ccemail,
    "subject": subject,
    "html": html,
    "text": htmltxt,
    "attachments": attachments,
    "from": config.nodemailer.email
  };


  app.transporter.sendMail(mailJSON, function (error, info) {
    if (error) {
      // console.log(error)
      // debugerror(error)
      // throw Error(error)
    } else {
      // debug(type, subject, emailinfo, clientinfo, template)
      return;
    }
  });
}

app.set('superSecret', config.secret)
app.disable("x-powered-by")

app.use(cors())
app.use(jsonParser)
app.use(urlEncoded)
app.use(compression())

app.all("/*", function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST,DELETE")
  res.header("Access-Control-Allow-Headers", "Cache-Control,Pragma, Origin, Authorization, Content-Type, X-Requested-With,X-XSRF-TOKEN, query,x-access-token")
  next()
})

const dbhelper = require("./model/dbhelper");
dbhelper.initialize();
/**
 * Oauth2Server
 * add oauth2Router.authorize middleware to any paths to protect them.
 */
let options = {
  app: app,
  executeQuery: dbhelper.executeQuery
  //   port:9090,

}
app.oauth2 = require('./services/oauth2');
app.oauth2.oauth2app(options);
//app.oauth2.oauth2app(app);
var multer = require('multer')
//  const os = require('os');

try {
  if (!fs.existsSync(temp_folder)) {
    fs.mkdirSync(temp_folder);
  }
  if (!fs.existsSync(actionPathFolder)) {
    fs.mkdirSync(actionPathFolder);
  }
  if (!fs.existsSync("sopraerror")) {
    fs.mkdirSync("sopraerror");
  }
  if (!fs.existsSync("templates")) {
    fs.mkdirSync("templates");
  }
 
} catch (error) {
  debugerror("error in make directory", error)
}
//  cb(null, require('fs').realpathSync(os.tmpdir()));
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, temp_folder);
  },

  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});
app.oauthTokens = {};
app.getOauthToken = function (host, port) {
  if (host !== undefined && port !== undefined) {
    const key = host + ":" + port;
    if (app.oauthTokens[key] !== undefined) {
      return app.oauthTokens[key];
    } else {
      app.oauthTokens[key] = new OauthToken(host, port);
      return app.oauthTokens[key];
    }
  }
}
app.upload = multer({ storage: storage });
//database initialize


//api docs
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

var manageRouter = require('./routes/manage')(app);
app.use('/manage', manageRouter);

var tasksRouter = require('./routes/tasks')(app);
app.use('/tasks', tasksRouter);




var mailer = require('./routes/mailer_r.js')(app);
app.use('/mailer', mailer);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.status(404).send({ status: 404, message: "Not found" });
  // next(createError(404));
});
// Routes
// console.log(app.oauth2)
// swaggerDocument.host = "localhost:" + app.oauth2.config.rpaAgentPort;
// app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const MonitorProcess = require("./action-paths/process");

// const StepCallback = require("./action-paths/callbackstep/callback");
// app.stepcallback = new StepCallback(app.getOauthToken);
const monitorProcess = new MonitorProcess(app.getOauthToken, app.SendEmail);
monitorProcess.start();

module.exports = app